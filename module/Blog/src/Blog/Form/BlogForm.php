<?php

namespace Blog\Form;

use Zend\Form\Form;

class BlogForm extends Form {
	public function __construct( $name = null ) {
		parent::__construct( 'blog' );

		$this->add( array(
			'name'    => 'title',
			'type'    => 'Text',
			'options' => array(
				'label' => 'Title',
			),
		) );
		$this->add( array(
			'name'    => 'post',
			'type'    => 'Text',
			'options' => array(
				'label' => 'Text',
			),
		) );
		$this->add( array(
			'name'    => 'tags',
			'type'    => 'Text',
			'options' => array(
				'label' => 'Tags',
			),
		) );
		$this->add( array(
			'name'       => 'submit',
			'type'       => 'Submit',
			'attributes' => array(
				'value' => 'Go',
				'id'    => 'submitbutton',
			),
		) );
	}
}