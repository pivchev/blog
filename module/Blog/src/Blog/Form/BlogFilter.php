<?php
namespace Blog\Form;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class BlogFilter extends InputFilter {
	public function __construct() {
		$this->add( array(
			'name'       => 'artist',
			'required'   => true,
			'filters'    => array(
				array( 'name' => 'StripTags' ),
				array( 'name' => 'StringTrim' ),
			),
			'validators' => array(
				array(
					'name'    => 'StringLength',
					'options' => array(
						'encoding' => 'UTF-8',
						'min'      => 1,
						'max'      => 100,
					),
				),
			),
		) );

		$this->add( array(
			'name'       => 'title',
			'required'   => true,
			'filters'    => array(
				array( 'name' => 'StripTags' ),
				array( 'name' => 'StringTrim' ),
			),
			'validators' => array(
				array(
					'name'    => 'StringLength',
					'options' => array(
						'encoding' => 'UTF-8',
						'min'      => 1,
						'max'      => 100,
					),
				),
			),
		) );

	}
}