<?php

namespace Blog\Form;

use Zend\Form\Form;

class CommentForm extends Form {
	public function __construct( $name = null ) {
		parent::__construct( 'blog' );

		$this->add( array(
			'name'    => 'name',
			'type'    => 'Text',
			'options' => array(
				'label' => 'Name',
			),
		) );
		$this->add( array(
			'name'    => 'email',
			'type'    => 'Text',
			'options' => array(
				'label' => 'Email:',
			),
		) );
		$this->add( array(
			'name'    => 'comment',
			'type'    => 'Text',
			'options' => array(
				'label' => 'Comment:',
			),
		) );
		$this->add( array(
			'name'       => 'submit',
			'type'       => 'Submit',
			'attributes' => array(
				'value' => 'Go',
				'id'    => 'submitbutton',
			),
		) );
	}
}