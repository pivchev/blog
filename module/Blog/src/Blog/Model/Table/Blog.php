<?php

namespace Blog\Model\Table;

use Zend\Db\TableGateway\TableGateway;

class Blog extends TableGateway {
	public function __construct( $adapter ) {
		parent::__construct( 'posts', $adapter );
	}

	public function displayPosts() {
		$adapter = $this->getAdapter();
		$result  = $adapter->query( 'SELECT * FROM `posts`' );

		return $result;
	}

	public function displayDetails( $id ) {
		$adapter = $this->getAdapter();
		$result  = $adapter->query( 'SELECT * FROM `posts` WHERE `post_id` = ?', array( $id ) );

		return $result;
	}

	public function displayComments( $id ) {
		$adapter = $this->getAdapter();
		$result  = $adapter->query( 'SELECT * FROM `comments` WHERE `bound_to_post_id` = ?', array( $id ) );

		return $result;
	}
}