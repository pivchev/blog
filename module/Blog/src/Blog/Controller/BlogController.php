<?php

namespace Blog\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Db\TableGateway\TableGateway;

use Blog\Form\BlogForm;
use Blog\Form\CommentForm;

use Blog\Form\BlogFilter;
use Blog\Form\CommentFilter;

use Blog\Model\Table\Blog;
use Zend\View\View;

class BlogController extends AbstractActionController {
	private $blogTable = null;

	// Retrieve - R
	public function indexAction() {

		return new ViewModel( array( 'rowset' => $this->getBlogTable()->select() ) );

	}

	// Create - C
	public function createAction() {
		$form    = new BlogForm();
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			//$form->setInputFilter( new BlogFilter() );
			$form->setData( $request->getPost() );
			if ( $form->isValid() ) {
				$data = $form->getData();
				unset( $data['submit'] );
				$this->getBlogTable()->insert( $data );

				return $this->redirect()->toRoute( 'blog/default' );
			}
		}

		return new ViewModel( array( 'form' => $form ) );
	}

	public function commentAction() {
		$id = $this->params()->fromRoute( 'id' );
		if ( ! $id ) {
			return $this->redirect()->toRoute( 'blog/default' );
		}
		$form    = new CommentForm();
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			//$form->setInputFilter( new CommentFilter() );
			$form->setData( $request->getPost() );
			if ( $form->isValid() ) {
				$data                     = $form->getData();
				$data["bound_to_post_id"] = $id;
				unset( $data['submit'] );
				$this->getBlogTable()->insert( $data );

				return $this->redirect()->toRoute( 'blog/default' );
			}
		}

		return new ViewModel( array( 'form' => $form ) );
	}

	public function detailsAction() {
		$id = $this->params()->fromRoute( 'id' );
		if ( ! $id ) {
			return $this->redirect()->toRoute( 'blog/default' );
		}

		return new ViewModel( array(
			'details'  => $this->getBlogTable()->displayDetails( $id ),
			'comments' => $this->getBlogTable()->displayComments( $id )
		) );
	}

	// Update - U
	public function updateAction() {
		$id = $this->params()->fromRoute( 'id' );
		if ( ! $id ) {
			return $this->redirect()->toRoute( 'blog/default' );
		}
		$form    = new BlogForm();
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			//$form->setInputFilter( new BlogFilter() );
			$form->setData( $request->getPost() );
			if ( $form->isValid() ) {
				$data = $form->getData();
				unset( $data['submit'] );
				$this->getBlogTable()->update( $data, array( 'post_id' => $id ) );

				return $this->redirect()->toRoute( 'blog/default' );
			}
		} else {
			$form->setData( $this->getBlogTable()->select( array( 'post_id' => $id ) )->current() );
		}

		return new ViewModel( array( 'form' => $form, 'id' => $id ) );
	}

	// Delete - D
	public function deleteAction() {
		$id = $this->params()->fromRoute( 'id' );
		if ( $id ) {
			$this->getBlogTable()->delete( array( 'post_id' => $id ) );
		}

		return $this->redirect()->toRoute( 'blog/default' );
	}

	public function getBlogTable() {
		if ( ! $this->blogTable ) {
			// $this->albumTable = new Album($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
			$this->blogTable = new Blog( // Album( //
				$this->getServiceLocator()->get( 'Zend\Db\Adapter\Adapter' )
//				new \Zend\Db\TableGateway\Feature\RowGatewayFeature('usr_id') // Zend\Db\RowGateway\RowGateway Object
//				ResultSetPrototype
			);
			//$this->albumTable = new Album( $this->getServiceLocator()->get( 'Zend\Db\Adapter\Adapter' ) );
		}

		return $this->blogTable;
	}
}