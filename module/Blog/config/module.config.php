<?php

namespace Blog;

return array(
	'controllers' => array(
        'invokables' => array(
            'Blog\Controller\Blog' => 'Blog\Controller\BlogController',
		),
	),
    'router' => array(
        'routes' => array(
			'blog' => array(
				'type'    => 'Literal',
				'options' => array(
					'route'    => '/blog',
					'defaults' => array(
						'__NAMESPACE__' => 'Blog\Controller',
						'controller'    => 'Blog',
						'action'        => 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'    => 'Segment',
						'options' => array(
							'route'    => '/[:controller[/:action[/:id]]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id'     	 => '[0-9]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),			
		),
	),
    'view_manager' => array(
        //'template_map' => array(
          //  'layout/Blog'           => __DIR__ . '/../view/layout/layout.phtml',
       //),
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view'
        ),
    ),	
);